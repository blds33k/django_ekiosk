from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'django_ekiosk.apiv1.api'
