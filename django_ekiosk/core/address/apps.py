from django.apps import AppConfig


class AddressConfig(AppConfig):
    name = 'django_ekiosk.core.address'
