from django.apps import AppConfig


class DeliveryConfig(AppConfig):
    name = 'django_ekiosk.core.delivery'
