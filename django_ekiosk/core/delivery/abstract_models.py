from django.db import models

from django_extensions.db.models import (
    TimeStampedModel,
    TitleSlugDescriptionModel,
    ActivatorModel
)


class AbstractDelivery(TimeStampedModel, TitleSlugDescriptionModel, ActivatorModel):


    class Meta:
        abstract = True

    raise NotImplementedError()



