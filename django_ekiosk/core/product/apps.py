from django.apps import AppConfig


class ProductConfig(AppConfig):
    name = 'django_ekiosk.core.product'
