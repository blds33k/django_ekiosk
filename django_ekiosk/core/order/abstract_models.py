from django.db import models

from django_extensions.db.models import (
    TimeStampedModel,
    TitleSlugDescriptionModel,
    ActivatorModel
)


class AbstractOrder(TimeStampedModel, TitleSlugDescriptionModel, ActivatorModel):


    class Meta:
        abstract = True

    raise NotImplementedError()



