from django.apps import AppConfig


class OrderConfig(AppConfig):
    name = 'django_ekiosk.core.order'
