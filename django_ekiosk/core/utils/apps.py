from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = 'django_ekiosk.core.utils'
